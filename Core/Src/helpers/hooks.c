/**
 * @file hooks.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Hooks and Handler Functions
 * @version 0.1
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <main.h>
#include <FreeRTOS.h>
#include <task.h>


/**
 * @brief FreeRTOS' Memory Manager's malloc() Failed Hook
 *
 * When configUSE_MALLOC_FAILED_HOOK is set in FreeRTOSConfig.h, this hook function is called when pvPortMalloc() is
 * unable to allocate the requested memory.
 *
 * @see https://www.freertos.org/a00110.html#configUSE_MALLOC_FAILED_HOOK
 */
void vApplicationMallocFailedHook( void ) {
  Error_Handler();
}

/**
 * @brief FreeRTOS' Task Stack Overflow Detection Hook
 *
 * When configCHECK_FOR_STACK_OVERFLOW is set in FreeRTOSConfig.h, this hook function is executed when FreeRTOS
 * detects an overflow of a task's stack.
 *
 * @param xTask offending task's handle
 * @param pcTaskName offending task's name
 *
 * @see https://www.freertos.org/Stacks-and-stack-overflow-checking.html
 */
void vApplicationStackOverflowHook( TaskHandle_t xTask, char *pcTaskName ) {
  Error_Handler();
}
