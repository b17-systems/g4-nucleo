/**
 * @file apps/led.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief LED Blinking App
 * @version 0.1
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <apps/main.h>
#include <apps/led.h>

#include <uavcan/node/Heartbeat_1_0.h>

static void LEDTask(void *param);
static void uselesslySerializeAndDeserializeHeartbeatMessages();


void startLEDTask(void) {
    static TaskHandle_t LEDTaskHandle = NULL;

    if (LEDTaskHandle != NULL) {
        // This function may only be called once
        Error_Handler();
        return;
    }

    // The task will directly enter running mode
    if (xTaskCreate(LEDTask, "LED Task", LED_TASK_STACK_SIZE, NULL, 1, &LEDTaskHandle) != pdPASS) {
        Error_Handler();
    }
}

static void LEDTask(void *param) {
    extern TIM_HandleTypeDef htim16;
    HAL_TIM_PWM_Start(&htim16, TIM_CHANNEL_1);

    volatile uint32_t *value = &htim16.Instance->CCR1;
    int increment            = -10;

    TickType_t now = xTaskGetTickCount();

    while (1) {
        if (*value >= 1000) {
            increment = -10;
            printf("down\r\n");
        } else if (*value <= 0) {
            increment = 10;
            printf("up\r\n");
        }

        *value += increment;

        uselesslySerializeAndDeserializeHeartbeatMessages();

        vTaskDelayUntil(&now, pdMS_TO_TICKS(10));
    }
}

static void uselesslySerializeAndDeserializeHeartbeatMessages() {
    uavcan_node_Heartbeat_1_0 beat = {.uptime = HAL_GetTick(),
                                      .health = (uavcan_node_Health_1_0){.value = uavcan_node_Health_1_0_CAUTION},
                                      .mode   = (uavcan_node_Mode_1_0){.value = uavcan_node_Mode_1_0_OPERATIONAL},
                                      .vendor_specific_status_code = 0xcf};

    const size_t bufferSize = uavcan_node_Heartbeat_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_;
    uint8_t buf[bufferSize];

    int8_t status = uavcan_node_Heartbeat_1_0_serialize_(&beat, buf, (size_t *const) & bufferSize);
    assert_param(status == NUNAVUT_SUCCESS);

    uavcan_node_Heartbeat_1_0 newBeat;
    status = uavcan_node_Heartbeat_1_0_deserialize_(&newBeat, buf, (size_t *const) & bufferSize);
    assert_param(status == NUNAVUT_SUCCESS);

    assert_param(beat.uptime == newBeat.uptime && beat.health.value == newBeat.health.value &&
                 beat.mode.value == newBeat.mode.value &&
                 beat.vendor_specific_status_code == newBeat.vendor_specific_status_code);
}
